

## Requisitos

- git clone git@gitlab.com:AldoGz/proyect-aws.git

Serverless
TypeScript
NodeJS 14.*
Dynamon 

### INSTALACIONES NPM
- npm i -> Instalacion de depencialas del proyectos


### REPOSTIORIO ALMACENADO EN AWS NPM
POST - https://48nh8phs37.execute-api.us-east-1.amazonaws.com/dev/task
    body:
        {
            "card_number": 4551038336650499,
            "cvv": 123,
            "expiration_month": "01",
            "expiration_year": "2027",
            "email": "soulaldo@gmail.com"
        }
GET  - https://48nh8phs37.execute-api.us-east-1.amazonaws.com/dev/token/1QQLBfHAbZV7Sg0W
    token : 1QQLBfHAbZV7Sg0W

```
.
├── src
│   ├── functions               # Lambda configuration and source code folder
│   │   ├── addTask
│   │   │   ├── addTask.ts      # `AddTask` lambda source code
│   │   │   ├── index.ts        # `AddTask` lambda Serverless configuration
│   │   │   ├── mock.json       # `AddTask` lambda input parameter, if any, for local invocation
│   │   │   └── schema.ts       # `AddTask` lambda input event JSON-Schema
│   │   ├── getTask
│   │   │   ├── getTask.ts      # `GetTask` lambda source code
│   │   │   ├── index.ts        # `GetTask` lambda Serverless configuration
│   │   │   ├── mock.json       # `GetTask` lambda input parameter, if any, for local invocation
│   │   │   └── schema.ts       # `GetTask` lambda input event JSON-Schema
│   │   │
│   │   └── index.ts            # Import/export of all lambda configurations
│   │
│   └── libs                    # Lambda shared code
│       └── apiGateway.ts       # API Gateway specific helpers
│       └── handlerResolver.ts  # Sharable library for resolving lambda handlers
│       └── lambda.ts           # Lambda middleware
│
├── package.json
├── serverless.ts               # Serverless service file
├── tsconfig.json               # Typescript compiler configuration
├── tsconfig.paths.json         # Typescript paths
└── webpack.config.js           # Webpack configuration
```

