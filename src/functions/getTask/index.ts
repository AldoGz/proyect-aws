//import schema from './schema';
import { handlerPath } from '@libs/handler-resolver';

export default {
  handler: `${handlerPath(__dirname)}/get.main`,
  events: [
    {
      http: {
        method: 'get',
        path: 'token/{token}',
        
      },
    },
  ],
};
