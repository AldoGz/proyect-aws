import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import schema from './schema';

const AWS = require('aws-sdk');

const getTask = async (event) => {
  try {
    const dynamodb = new AWS.DynamoDB.DocumentClient();
    const createdAt = new Date();
    const now = createdAt.getTime();
    const token = event.pathParameters.token;

    const getAll = await dynamodb.scan({
      TableName: 'TaskTable'
    }).promise();

    const { Items } = getAll;

    const token_activo = Items.filter(e => e.token == token && e.activo);

    if( token_activo.length > 0 ){
      const [ resultado ] = token_activo;
      const { id, card_number, email, expiration_month, expiration_year,expirationAt } = resultado;
      if( now > expirationAt ){
        return formatJSONResponse({
          message: "Su token expiracion ya caduco"
        });
      }

      const newData = {
        id,
        card_number,
        expiration_month,
        expiration_year,
        email
      }

      return formatJSONResponse({
        message: newData
      });
    }else{
      return formatJSONResponse({
        message: "No existe este token"
      });
    }
   
  } catch (e) {
    return formatJSONResponse({
      status: 500,
      message: e
    });
  }

};

export const main = middyfy(getTask);

