import schema from './schema';
import { handlerPath } from '@libs/handler-resolver';

export default {
  handler: `${handlerPath(__dirname)}/addTask.main`,
  events: [
    {
      http: {
        method: 'post',
        path: 'task',
        request: {
          schemas: {
            'application/json': schema,
          },
        },
      },
    },
  ],
};
