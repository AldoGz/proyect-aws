import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';
import schema from './schema';

const { v4 } = require('uuid');
const AWS = require('aws-sdk');

const addTask: ValidatedEventAPIGatewayProxyEvent<typeof schema> = async (event) => {
  try {
    const dynamodb = new AWS.DynamoDB.DocumentClient();
    const id = v4();
    const { card_number, cvv, expiration_month, expiration_year, email } = event.body;
    const createdAt = new Date();
    const anio_actual = createdAt.getFullYear();
    const anio_expiration = anio_actual + 5;
    const dominios = ["gmail.com", "hotmail.com", "yahoo.es"];
    const fecha_expiration = new Date();
    fecha_expiration.setMinutes(fecha_expiration.getMinutes() + 15);

    const getAll = await dynamodb.scan({
      TableName: 'TaskTable'
    }).promise();

    const { Items } = getAll;

    if (Items.length > 0) {
      const [resultado] = Items.filter(e => e.card_number == card_number && e.activo);

      if (resultado != undefined) {
        if (resultado.expirationAt >= createdAt.getTime()) {
          return formatJSONResponse({
            message: `Este token expiration 15 minutos`
          });
        }

        await dynamodb.update({
          TableName: 'TaskTable',
          Key: { id: resultado.id },
          UpdateExpression:
            "set #activo = :status",
          ExpressionAttributeNames: {
            "#activo": "activo",
          },
          ExpressionAttributeValues: {
            ":status": false,
          },
          ReturnValues: "ALL_NEW",
        }).promise();
      }
    }

    const generarToken = () => {
      var text = "";
      var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
      var i = 1;
      while (i <= 16) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
        i++;
      }
      return text
    }

    if (cvv.toString().length < 3 || cvv.toString().length > 4) {
      return formatJSONResponse({
        message: `El parametro CVV debe tener 3 o 4 numeros`
      });
    }

    if (expiration_month.length < 1 || expiration_month.length > 2) {
      return formatJSONResponse({
        message: `El parametro expiration month debe tener 1 o 2 caracteres`
      });
    }

    if (parseInt(expiration_month) < 1 || parseInt(expiration_month) > 12) {
      return formatJSONResponse({
        message: `El parametro expiration month debe estar entre un rango de 1 o 12`
      });
    }

    if (expiration_year.length < 1 || expiration_year.length > 4) {
      return formatJSONResponse({
        message: `El parametro expiration month debe tener 1 o 4 caracteres`
      });
    }

    if (parseInt(expiration_year) < anio_expiration || parseInt(expiration_year) > anio_expiration) {
      return formatJSONResponse({
        message: `El parametro expiration year debe tener un rango de 5 años`
      });
    }

    const validarDominio = (valor: string) => {
      let flag = false;
      const array = valor.split("@");
      const [, dominio] = array;

      dominios.forEach(element => {
        if (element == dominio) {
          flag = true;
        }
      });
      return flag;
    }

    const validarEmail = (valor: string) => {
      return /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/.test(valor);
    }

    if (!validarEmail(email)) {
      return formatJSONResponse({
        message: `El parametro email debe ser un correo electronico valido`
      });
    }

    if (!validarDominio(email)) {
      return formatJSONResponse({
        message: `El parametro email debe tener como dominio “gmail.com”, “hotmail.com” y “yahoo.es”`
      });
    }
    /* 
      const luhn = ( value : string ) => {
        if (/[^0-9-\s]+/.test(value)) return false;
    
        var nCheck = 0, nDigit = 0, bEven = true;
        value = value.replace(/\D/g, "");
    
        for (var n = value.length - 1; n >= 0; n--) {
          var cDigit = value.charAt(n),
            nDigit = parseInt(cDigit, 10);
    
          if (bEven) {
            if ((nDigit *= 2) > 9) nDigit -= 9;
          }
    
          nCheck += nDigit;
          bEven = !bEven;
        }
        const sum = 1000 - nCheck;
    
        return ((sum % 10 == 0) && (sum != 0));
      } */



    const newTask = {
      id,
      card_number,
      cvv,
      expiration_month,
      expiration_year,
      email,
      token: generarToken(),
      activo: true,
      expirationAt: fecha_expiration.getTime()
    }

    await dynamodb.put({
      TableName: 'TaskTable',
      Item: newTask
    }).promise();

    return formatJSONResponse({
      message: newTask
    });
  } catch (e) {
    return formatJSONResponse({
      status: 500,
      message: e
    });
  }

};

export const main = middyfy(addTask);
